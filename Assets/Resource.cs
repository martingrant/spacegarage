﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : MonoBehaviour {

    public int type;

	// Use this for initialization
	void Start () {
        type = 0;
        GetComponent("mesh").GetComponent<MeshRenderer>().material = (Material)Resources.Load("rockmat2", typeof(Material));
        switch (type) 
        {
            case 0:
                this.GetComponent<Renderer>().material = (Material)Resources.Load("rockmat1", typeof(Material));
                break;
            case 1:
                this.GetComponent<Renderer>().material = (Material)Resources.Load("rockmat2", typeof(Material));
                break;
            default:
                break;
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
