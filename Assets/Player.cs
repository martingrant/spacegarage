﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    Rigidbody rb;

    private Vector3 vel;
    public int speed;
    private int resources1;
    private int resources2;

    public Text resText1;
    public Text resText2;
    public Slider slider;
    private float sliderIncrement;

    public float timeToCrouch = 3.0F;
    float crouchTimer = 0;

    private float distToGround;
    BoxCollider boxCollider;


    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();

        vel = new Vector3(0, 0, 0);
        speed = 5;
        resources1 = 0;
        resources2 = 0;

        setResourcesText();
        slider.gameObject.SetActive(false);
        sliderIncrement = 0.007f;

        boxCollider = GetComponent<BoxCollider>();
        distToGround = boxCollider.bounds.extents.y;
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + vel * Time.fixedDeltaTime);

    }

    // Update is called once per frame
    void Update () {


        Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        vel = input.normalized * speed;



        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f))
            {
                Vector3 up = new Vector3(0, 5, 0);
                rb.AddForce(up, ForceMode.Impulse);
            }
        }


        if (Input.GetKeyUp(KeyCode.C))
        {
            crouchTimer = 0;
            resetSlider();
        }

    }

    void OnTriggerEnter(Collider other)
    {

    }

    void OnCollisionEnter(Collision col)
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "resource")
        {
            if (Input.GetKey(KeyCode.C))
            {
                slider.gameObject.SetActive(true);
                slider.value += sliderIncrement;
                crouchTimer += Time.deltaTime;

                if (crouchTimer > timeToCrouch)
                {
                    Destroy(other.gameObject);
                    resources1++;
                    setResourcesText();
                    resetSlider();
                }
            }
        }

        if (other.gameObject.tag == "resource2")
        {
            if (Input.GetKey(KeyCode.C))
            {
                slider.gameObject.SetActive(true);
                slider.value += sliderIncrement;
                crouchTimer += Time.deltaTime;

                if (crouchTimer > timeToCrouch)
                {
                    Destroy(other.gameObject);
                    resources2++;
                    setResourcesText();
                    resetSlider();
                }
            }
        }

    }

    void setResourcesText() 
    {
        resText1.text = "Resources1: " + resources1.ToString();
        resText2.text = "Resources2: " + resources2.ToString();
    }

    void resetSlider()
    {
        slider.gameObject.SetActive(false);
        slider.value = 0.0f;
    }

 

}
